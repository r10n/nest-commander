#!/usr/bin/env bash

# Upgrade all dependencies

# Note: This script depends on the following programs:
# - `pnpm`;
# - `jq`;
# - `grep`;
# - `ls`;
# - `rm`;
# - `realpath`;
# - `dirname`;

repo_root="$(realpath "$(dirname "$(dirname "$0")")")"

# TODO: Check which NPM package manager is to be used.

# TODO: Check if `nx` is actually installed in the current repository.

# Upgrade Nx packages
echo 'Migrating Nx packages ...'
pnpm nx migrate latest

# Upgrade dependencies and dev dependencies
# TODO: When `ts-jest` is updated, update also `@types/jest` and `jest` to the same version.
for p in $(jq -r '.dependencies + .devDependencies | to_entries[] | [.key][]' "$repo_root/package.json" | grep -v '^nx$\|^@nx/.*$\|^@nrwl/.*$'); do
	# Note: Nest currently supports only `cache-manager@4.1.0` and `cache-manager-redis-store@2.0.0`.
	case "$p" in
		cache-manager|@types/cache-manager)
			tag='4'
		;;
		# Note: When `cache-manager` is updated, update also `@types/cache-manager` and `cache-manager-redis-store` with `@types/cache-manager-redis-store`.
		cache-manager-redis-store|@types/cache-manager-redis-store)
			tag='2'
		;;
		# Note: Don't upgrade `class-validator` to a different version that than the version supported by `@nestjs/mapped-types`.
		class-validator)
			tag="$(pnpm info @nestjs/mapped-types peerDependencies | grep -Po "'class-validator': .*[ \^]+\K[\d.]+(?=',$)")"
		;;
		*)
			tag='latest'
	esac

	echo "Migrating '$p@$tag' ..."
  pnpm nx migrate "$p@$tag"
done

# Running migrations if there are any
if [ -f "$repo_root/migrations.json" ]; then
	pnpm nx migrate --run-migrations
	rm "$repo_root/migrations.json"
fi

# Consider checking `package.json` if the changes make sense; if yes, install the updated dependencies
# TODO: `--package-lock` is not recognised by `pnpm@8.6.5`.
# pnpm i --package-lock
pnpm i
