import {CommandFactory} from 'nest-commander'

import {NewModule} from './gnew/new.module.js'

async function bootstrap(): Promise<void> {
	const app = await CommandFactory.createWithoutRunning(NewModule)
	// gnew.useLogger(gnew.get(OgmaService))
	await CommandFactory.runApplication(app)
}

bootstrap().catch(e => console.log(e))
