import {OgmaLogger, OgmaService} from '@ogma/nestjs-module'
import {Command as cmd} from 'commander'
import {Command, CommandRunner, InjectCommander, Option} from 'nest-commander'
import {execa} from "execa";

@Command({
	description: 'Create a new Git repository',
	name: 'new',
	options: {isDefault: true},
})
export class NewCommand extends CommandRunner {
	constructor(
		@InjectCommander() private readonly commander: cmd,
		@OgmaLogger(NewCommand.name) private readonly logger: OgmaService
	) {
		super()
	}

	@Option({
		description: 'xxx',
		flags: '-r, --rrr <string>',
		name: 'rrr',
		required: false
	})
	parseIp(value: string): string {
		return value
	}

	// async run(inputs: string[], options: PlcCommandOptionsDto): Promise<void> {
	async run(inputs: string[], options: Record<string, unknown>): Promise<void> {
		// this.logger.info(`command :`, {inputs, options})
		console.log(`command :`, {inputs, options})

    const data = await execa('echo', ['test'], {all: true})
    // this.logger.info(data)
    console.log({stdAll: data.all, exitCode: data.exitCode})
	}
}
