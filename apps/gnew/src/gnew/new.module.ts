import { Module } from '@nestjs/common';
import {NewCommand} from "./command/new.command.js";
import {OgmaModule} from '@ogma/nestjs-module'

@Module({
  imports: [
    OgmaModule.forRoot({
			application: 'gnew',
			color: true,
			each: true,
			json: false,
			logLevel: 'DEBUG'
		}),
		OgmaModule.forFeatures([
			NewCommand.name,
		])
  ],
  providers: [NewCommand],
})
export class NewModule {}
